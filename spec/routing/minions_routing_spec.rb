require "rails_helper"

RSpec.describe MinionsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/minions").to route_to("minions#index")
    end

    it "routes to #new" do
      expect(:get => "/minions/new").to route_to("minions#new")
    end

    it "routes to #show" do
      expect(:get => "/minions/1").to route_to("minions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/minions/1/edit").to route_to("minions#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/minions").to route_to("minions#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/minions/1").to route_to("minions#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/minions/1").to route_to("minions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/minions/1").to route_to("minions#destroy", :id => "1")
    end
  end
end
