# Preview all emails at http://localhost:3000/rails/mailers/reservation_mailer
class ReservationMailerPreview < ActionMailer::Preview
	def reservation_admin		
		ReservationMailer.new_reservation_admin(Reservation.first)
	end
	def reservation_user
		ReservationMailer.new_reservation_user(Reservation.first)
	end
end
