require 'rails_helper'

RSpec.describe "home/index.html.haml", type: :view do
  it "display a welcome message" do 
  	render
  	expect(rendered).to have_text('Bem vindo a minion store')
  end
end
