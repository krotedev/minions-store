require 'rails_helper'

RSpec.describe "reservations/new", type: :view do
  before(:each) do
    assign(:reservation, Reservation.new(
      :minion => nil,
      :user => nil,
      :message => "MyText"
    ))
  end

  it "renders new reservation form" do
    render

    assert_select "form[action=?][method=?]", reservations_path, "post" do

      assert_select "input[name=?]", "reservation[minion_id]"

      assert_select "input[name=?]", "reservation[user_id]"

      assert_select "textarea[name=?]", "reservation[message]"
    end
  end
end
