require 'rails_helper'

RSpec.describe "reservations/edit", type: :view do
  before(:each) do
    @reservation = assign(:reservation, Reservation.create!(
      :minion => nil,
      :user => nil,
      :message => "MyText"
    ))
  end

  it "renders the edit reservation form" do
    render

    assert_select "form[action=?][method=?]", reservation_path(@reservation), "post" do

      assert_select "input[name=?]", "reservation[minion_id]"

      assert_select "input[name=?]", "reservation[user_id]"

      assert_select "textarea[name=?]", "reservation[message]"
    end
  end
end
