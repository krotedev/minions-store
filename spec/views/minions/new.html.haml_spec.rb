require 'rails_helper'

RSpec.describe "minions/new", type: :view do
  before(:each) do
    assign(:minion, Minion.new(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new minion form" do
    render

    assert_select "form[action=?][method=?]", minions_path, "post" do

      assert_select "input[name=?]", "minion[name]"

      assert_select "textarea[name=?]", "minion[description]"
    end
  end
end
