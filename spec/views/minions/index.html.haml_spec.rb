require 'rails_helper'

RSpec.describe "minions/index", type: :view do
  before(:each) do
    assign(:minions, [
      Minion.create!(
        :name => "Name",
        :description => "MyText"
      ),
      Minion.create!(
        :name => "Name",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of minions" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
