require 'rails_helper'

RSpec.describe "minions/edit", type: :view do
  before(:each) do
    @minion = assign(:minion, Minion.create!(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit minion form" do
    render

    assert_select "form[action=?][method=?]", minion_path(@minion), "post" do

      assert_select "input[name=?]", "minion[name]"

      assert_select "textarea[name=?]", "minion[description]"
    end
  end
end
