require 'rails_helper'

RSpec.describe "minions/show", type: :view do
  before(:each) do
    @minion = assign(:minion, Minion.create!(
      :name => "Name",
      :description => "MyText",
      :image => Rails.root+"/storage/minionFoto/default.jpg"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
  end
end
