require 'rails_helper'

RSpec.describe Minion::ReservationsController, type: :controller do
  let(:user_attributes){
    {
      name: FFaker::Name.name,
      phone: FFaker::PhoneNumber.phone_number,
      email: FFaker::Internet.email,
      password: Devise.friendly_token[8,20]
    }
  }
  let(:minion_attributes){
    {
      name: FFaker::Name.name,
      description: FFaker::AWS.product_description,     
    }
  }      
  describe "GET #index" do
    before(:each) do 
      @user = User.create! user_attributes      
      @minion = Minion.create! minion_attributes          
    end

    it "returns a success response" do      
      get :index, params: {minion_id: @minion.to_param}
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    before(:each) do 
      @user = User.create! user_attributes      
      @minion = Minion.create! minion_attributes      
    end
    it "returns a success response" do
      reservation = Reservation.create!(minion: @minion, user: @user, message: FFaker::Lorem.paragraph)
      get :show, params: {minion_id: @minion.to_param, id: reservation.to_param}
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    before(:each) do 
      @user = User.create! user_attributes      
      @minion = Minion.create! minion_attributes      
    end
    it "returns a success response" do
      get :new, params: {minion_id: @minion.to_param}
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    before(:each) do 
      @user = User.create! user_attributes      
      @minion = Minion.create! minion_attributes      
    end
    it "returns a success response" do      
      reservation = @minion.reservations.create!(user: @user, message: FFaker::Lorem.paragraph)
      get :edit, params: {minion_id: @minion.to_param, id: reservation.to_param}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    before(:each) do 
      @user = User.create! user_attributes      
      @minion = Minion.create! minion_attributes      
    end
    context "with valid params" do
      let(:valid_attributes){
        {          
          user_id: @user.id,
          message: FFaker::Lorem.paragraph
        }
      }
      it "creates a new Reservation" do
        expect {
          post :create, params: {minion_id: @minion.to_param, reservation: valid_attributes}
        }.to change(Reservation, :count).by(1)
      end

      it "redirects to the created reservation" do
        post :create, params: {minion_id: @minion.to_param, reservation: valid_attributes}
        expect(response).to redirect_to(minion_reservations_path(@minion.to_param, Reservation.last))
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {reservation: invalid_attributes}, session: valid_session
        expect(response).to be_successful
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested reservation" do
        reservation = Reservation.create! valid_attributes
        put :update, params: {id: reservation.to_param, reservation: new_attributes}, session: valid_session
        reservation.reload
        skip("Add assertions for updated state")
      end

      it "redirects to the reservation" do
        reservation = Reservation.create! valid_attributes
        put :update, params: {id: reservation.to_param, reservation: valid_attributes}, session: valid_session
        expect(response).to redirect_to(reservation)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        reservation = Reservation.create! valid_attributes
        put :update, params: {id: reservation.to_param, reservation: invalid_attributes}, session: valid_session
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested reservation" do
      reservation = Reservation.create! valid_attributes
      expect {
        delete :destroy, params: {id: reservation.to_param}, session: valid_session
      }.to change(Reservation, :count).by(-1)
    end

    it "redirects to the reservations list" do
      reservation = Reservation.create! valid_attributes
      delete :destroy, params: {id: reservation.to_param}, session: valid_session
      expect(response).to redirect_to(reservations_url)
    end
  end

end
