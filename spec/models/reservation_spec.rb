require 'rails_helper'

RSpec.describe Reservation, type: :model do
	let(:user_attributes){
		{
			name: FFaker::Name.name,
			phone: FFaker::PhoneNumber.phone_number,
			email: FFaker::Internet.email,
			password: Devise.friendly_token[8,20]
		}
	}
	let(:minion_attributes){
		{
			name: FFaker::Name.name,
			description: FFaker::AWS.product_description,			
		}
	}
	context "Create a user, a minion and try to create a reservation" do 
		before(:each) do 
			@user = User.create! user_attributes			
			@minion = Minion.create! minion_attributes					
		end
		it "Validates the presence of user" do 
			reservation = Reservation.new(minion: @minion, message: FFaker::Lorem.paragraph).save
			expect(reservation).to eq(false)
		end
		it "Validates the presence of minion" do 
			reservation = Reservation.new(user: @user, message: FFaker::Lorem.paragraph).save
			expect(reservation).to eq(false)
		end
		it "Should sucessfuly create a reservation" do 
			reservation = Reservation.new(minion: @minion, user: @user, message: FFaker::Lorem.paragraph).save
			expect(reservation).to eq(true)
		end
	end
end
