require 'rails_helper'

RSpec.describe User, type: :model do
  context "User creation " do 
  	let(:valid_attributes){
  		{
  			name: FFaker::Name.name,
  			phone: FFaker::PhoneNumber.phone_number,
  			email: FFaker::Internet.email,
  			password: Devise.friendly_token[8,20]
  		}
  	}
  	it "Should validate the presence of name" do
  		user = User.new(valid_attributes.except(:name)).save
  		expect(user).to eq(false)
  	end
  	it "Should validate the presence of email" do
  		user = User.new(valid_attributes.except(:email)).save
  		expect(user).to eq(false)
  	end
  	it "Validates that the new user has a is_admin equals false" do 
  		user = User.new(valid_attributes)
  		expect(user.is_admin).to eq(false)
  	end
  	it "Should create a new user" do
  		user = User.new(valid_attributes).save
  		expect(user).to eq(true)
  	end
  	it "creates a new user with is_admin true" do
  		user = User.new(valid_attributes.merge(:is_admin => true))
  		expect(user.is_admin).to eq(true)
  	end
  end
end
