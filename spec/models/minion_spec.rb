require 'rails_helper'

RSpec.describe Minion, type: :model do
	describe "Validates the minion factory" do 
	  let(:valid_attributes){
	  	{	  
		  	name: FFaker::Name.name,
		  	description: FFaker::AWS.product_description,
		  	image: Rails.root+"/storage/minionFoto/default.jpg"		  	
	  	 }
	  }
	  it "Should validate the presence of the name" do 
	  	minion = Minion.new(valid_attributes.except(:name)).save
  		expect(minion).to be false
	  end
	  it "Should validate the presence of the description" do 
	  	minion = Minion.new(valid_attributes.except(:description)).save
  		expect(minion).to be false
	  end
	  it "create a minion" do 
	  	minion = Minion.new(valid_attributes).save
  		expect(minion).to be true
	  end
	end
end
