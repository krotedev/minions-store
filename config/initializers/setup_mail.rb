ActionMailer::Base.smtp_settings = {
	:address 								=> "smtp.gmail.com",
	:port 									=> 587,
	:domain 								=> "produtividade.com.br",
	:user_name 							=> ENV['meinelog_mail_username'],
	:password 							=> ENV['meinelog_mail_password'],
	:authentication 				=> "plain",
	:enable_starttls_auto 	=> true
}
ActionMailer::Base.default_url_options[:host] = "aqueous-wildwood-91372.herokuapp.com"