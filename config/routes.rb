Rails.application.routes.draw do  
  
  root "minions#index"
	get 'home/index'
  resources :minions do 
  	resources :reservations	
  end
  devise_for :users, :controllers => { registrations: 'registrations' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'myreservations' => "reservations#myreservations", as: "myreservations"
end
