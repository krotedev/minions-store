class Minion < ApplicationRecord
	has_one_attached :image
	validates_presence_of :name, :description
	has_many :reservations, dependent: :destroy
end
