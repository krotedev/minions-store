class ReservationMailer < ApplicationMailer
	
	def new_reservation_user(reservation)
		@reservation = reservation
		mail(:to => reservation.user.email, :subject => "Sua reserva foi efetuada com sucesso")
	end
	
	def new_reservation_admin(reservation)
		@reservation = reservation
		mail(:to => "raquel@inventosdigitais.com.br", :subject => "Uma nova reserva foi efetuada na Minion Store", :bcc => "oliveira.desu@gmail.com")
	end
end
