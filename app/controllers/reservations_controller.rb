class ReservationsController < ApplicationController
  before_action :set_minion, except:[:myreservations]
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]  
  before_action :authenticate_user!

  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = current_user.reservations.all
  end
  def myreservations
    @reservations = current_user.reservations.all
  end
  # GET /reservations/1
  # GET /reservations/1.json
  def show
  end

  # GET /reservations/new
  def new
    # @reservation = @minion.reservations.new
    @reservation = current_user.reservations.build
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create    
    @reservation = @minion.reservations.new(reservation_params)
    @reservation.user = current_user    
    respond_to do |format|
      if @reservation.save
        ReservationMailer.new_reservation_user(@reservation).deliver
        ReservationMailer.new_reservation_admin(@reservation).deliver
        format.html { redirect_to root_path, notice: t('reservations.reservation_success') }
        format.json { render :show, status: :created, location: root_path }
      else
        format.html { render :new }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation.destroy
    respond_to do |format|
      format.html { redirect_to reservations_url, notice: 'Reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # get the minion id
    def set_minion
      @minion = Minion.find(params[:minion_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = @minion.reservations.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.require(:reservation).permit(:minion_id, :user_id, :message)
    end

end
