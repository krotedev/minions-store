json.extract! reservation, :id, :minion_id, :user_id, :message, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
