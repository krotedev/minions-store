json.extract! minion, :id, :name, :description, :created_at, :updated_at
json.url minion_url(minion, format: :json)
